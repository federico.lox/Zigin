# TODO

- [x] Zig relase from: https://ziglang.org/download/index.json - `jq '.master."x86_64-linux".tarball'`
- [x] Structure bash code properly
- [x] zls release from: https://zig.pm/zls/downloads/x86_64-linux/bin/zls
- [x] Runs on Mac OS (bash v3) via zsh
- [x] Restructure flow around commands (install, update, help, etc.)
- [x] Add error wrappers for cat, tar and ln usages
- [x] Normalize use of returns and exit codes
- [x] Add `set -u` at the top 
- [x] zigmod release from: https://api.github.com/repos/nektro/zigmod/releases - `jq '.[0].assets[] | select(.name == "zigmod-x86_64-linux") | .browser_download_url'`
- [ ] urlencode target in urls `jq -R -r @uri <<<“$1”`
- [ ] Use [shellcheck](https://www.shellcheck.net)
- [ ] Add `cleanup` command to clear caches
- [ ] Add `uninstall` command to remove the `.zigin` folder
- [ ] Add `update self` command, downloads directly from `https://codeberg.org/federico.lox/Zigin/raw/branch/main/zigin` 
- [ ] Add language server config to `.replit`
- [ ] Add *interactive mode* (default) asking Y/N decision at every major step, override with a flag to force Y answers for automation
- [ ] Verify checksums where provided before installing
- [ ] Restructure around the concept of *targets* and *distributions* and *tools*
  - [ ] Add `target` command
      - [ ] plain shows detected target for the machine
      - [ ] `target list` shows  all supported targets for [Zig's compiler builds](https://ziglang.org/download/index.json) 
  - [ ] Add `distribution` command
      - [ ] `distribution tools` lists al installed tools for the current distribution and their versions
- [ ] for `zls` use `curl —-location —-head <url>` to get last-modified header in absence of json data about latest version
    - [ ] Use [jq’ RegEx engine](https://stedolan.github.io/jq/manual/#RegularexpressionsPCRE) to extract the date from the header 
 
---


# Notes

A list of thoughts that, once refined, will be moved to [README](README.md).


## Core principles

1. Ease of installation - sourcing Zigin should be easier than installing the sum of the tools it supports, and the effort should pay off over time by saving big on maintaining the toolchain up-to-date/switching versions.
2. Work on all 3 major OSes - developing on Windows is a thing, deal with it.
3. Avoid dependencies - single, self-contained executable that works out of the box; if dependencies are unavoidable, then those too need to abide by principles #1 and #2.
4. Opinionated toolchain - where there’s more than one option for a  specific type of tool, support only the one that is both popular with the Zig community AND abides by all the principles above.

 
## Organisation of concepts

- *target* => *distribution* => *tools*
- each *tool* has a name, version and an URL for the artefacts to install
- for each tool there’s also a *repository* URL from where to get a list of versions and targets
  - In later iterations the mapping between the three entities could be provided via a static json mapping built ahead-of-time by an automated pipeline (e.g. on Circle CI) which could be in part manually curated for defining versioned distributions in a reliable way as well as allowing for better testing of bundled distributions


## Flow of data

1. Fetch the mapping dataset
2. Build a representation of available targets and distributions available for each target
3. Fetch and install the tools bound to the selected pair of target and distribution
4. Validate installation
5. Done!


## Choice of language

Currently built with portable shell constructs, trying to limit number of dependencies and if a dependency is needed then choosing a popular one that is easy to install on all the 3 major OSes.

The ideal state would be to build a fully static binary that has no dependencies and is portable across the 3 major OSes. This can be achieved today using *Go* and potentially solvable with *Zig* itself as far as stable JSON, HTTP client and LZMA modules become available and are portable (including their native dependencies, if any, allowing static linking in all supported OSes).

Will likely stick with Zigin being distributed as a shell script until v1.0 and delay taking this step until a major blocker pops up along the way that cannot be solved sticking to the core principles.


## Dependencies

`bash` is available out of the box on GNU Linux. On Mac OSX the version of Bash is too old (3.x), so Zigin tries to be compatible with `zsh`, which is the default shell on that OS. On Windows bash can be obtained using either [Git for Windows](https://gitforwindows.org) (which also includes all the other dependencies listed below) or using a GNU Linux distribution via [WSL/WSL2](https://learn.microsoft.com/en-us/windows/wsl/about).

`curl` and `tar` are available out of the box on GNU Linux, Mac OSX and [Windows 10](https://techcommunity.microsoft.com/t5/containers/tar-and-curl-come-to-windows/ba-p/382409).

`jq` requires separate installation but is available through the package managers of each of the 3 OSes (on Homebrew for Mac OSX and on Winget, Scoop and Chocolatey for Windows).