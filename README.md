# Zigin

The [Zig](https://ziglang.org) toolchain installer.

Zigin automates the installation of the official Zig compiler and several community tools to improve the out-of-the-box
Zig developer experience:

- [zls](https://github.com/zigtools/zls/) - The Zig Language Server
- [zigmod](https://nektro.github.io/zigmod/) - An ergonomic package manager for Zig modules from the community

More will be added in upcoming releases.

## Installation

Download the latest version of [zigin](https://codeberg.org/federico.lox/Zigin/raw/branch/main/zigin) and either run it
as a script (`sh /path/to/zigin`) or as an executable (after running `chmod +x /path/to/zigin`).

### System requirements:

- Operating system: GNU Linux, Mac OSX, Windows 10 or higher
- Bash version 4 or higher OR zsh (other shell environments might work but are not actively tested)
  - On Windows use [Git for  Windows](https://gitforwindows.org) or
    [WSL/WSL2](https://learn.microsoft.com/en-us/windows/wsl/)
- [jq](https://stedolan.github.io/jq/download/) for JSON parsing

## Usage

`zigin <command>`

### Commands:

- `help` - Displays the help screen.
- `install` - Install the latest (development) version of the toolchain.
- `update` - An alias to the install in this version.  
